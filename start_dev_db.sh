user=${POSTGRES_USER:-talkietalkie}
password=${POSTGRES_PASSWORD:-talkietome}

docker start talkietalkie_pg

if [ $? -eq 0 ]; then
  exit 0
else
  docker run -d --name talkietalkie_pg -v talkietalkie_db_data:/var/lib/postgresql/data -e POSTGRES_USER=${user} -e POSTGRES_PASSWORD=${password} -p 5432:5432 postgres:12
fi
