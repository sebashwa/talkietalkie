![logo](logo.svg)

# talkietalkie

spark dialogues about anything on the web

# Development

To start your Phoenix server:

  * Run `./start_dev_db.sh` to start a development postgres inside a docker container
  * Setup the project with `mix setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
