defmodule Talkietalkie.ChatTest do
  use ExUnit.Case, async: true

  alias Talkietalkie.Accounts.User
  alias Talkietalkie.Convos.{Chat}

  describe "partner_for/2" do
    test "returns responder if user is requester" do
      user = %User{id: 1}
      responder = %User{id: 2}
      chat = %Chat{requester: user, responder: responder}

      assert Chat.partner_for(chat, user) == responder
    end

    test "returns requester if user is responder" do
      user = %User{id: 1}
      requester = %User{id: 2}
      chat = %Chat{requester: requester, responder: user}

      assert Chat.partner_for(chat, user) == requester
    end
  end
end
