defmodule Talkietalkie.ConvosTest do
  use Talkietalkie.DataCase, async: true

  alias Talkietalkie.Convos
  alias Talkietalkie.Accounts.User
  alias Talkietalkie.Convos.{Content, Talkie, Chat, Message}

  import Talkietalkie.ConvosFixtures
  import Talkietalkie.AccountsFixtures

  describe "list_user_talkies/1" do
    test "returns all talkies of a given user" do
      owner = user_fixture()
      %Talkie{id: id} = talkie_fixture(owner)
      talkie_fixture()
      assert [%Talkie{id: ^id}] = Convos.list_user_talkies(owner)
    end

    test "orders talkies by last message of related chat" do
      user = user_fixture()
      %Talkie{id: id1} = talkie_fixture(user)
      %Talkie{id: id2} = talkie2 = talkie_fixture(user)
      %Talkie{id: id3} = talkie_fixture(user)

      chat = chat_fixture(user_fixture(), talkie2)
      message_fixture(chat)

      assert [%Talkie{id: ^id2}, %Talkie{id: ^id1}, %Talkie{id: ^id3}] =
               Convos.list_user_talkies(user)
    end

    test "adds siblings counts to talkies" do
      talkie = talkie_fixture()
      talkie_fixture(user_fixture(), talkie.content)

      assert hd(Convos.list_user_talkies(talkie.user)).siblings_count == 1
    end

    test "it does not consider unrelated talkies for siblings counts" do
      talkie = talkie_fixture()
      talkie_fixture()

      assert hd(Convos.list_user_talkies(talkie.user)).siblings_count == 0
    end

    test "adds chats count and unread messages count to talkies" do
      talkie = talkie_fixture()
      sibling_talkie = talkie_fixture(user_fixture(), talkie.content)

      talkie_chat = chat_fixture(user_fixture(), talkie)
      message_fixture(talkie_chat, talkie.user)
      sibling_chat = chat_fixture(talkie.user, sibling_talkie)
      message_fixture(sibling_chat, sibling_talkie.user)

      user_talkies = Convos.list_user_talkies(talkie.user)
      assert hd(user_talkies).chats_count == 2
      assert hd(user_talkies).unread_messages_count == 2
    end

    test "does not consider read messages for unred messages count" do
      user = user_fixture()
      talkie = talkie_fixture(user)

      chat = chat_fixture(user_fixture(), talkie)
      message_fixture(chat, user)
      Convos.mark_messages_as_read(chat, user)

      user_talkies = Convos.list_user_talkies(user)
      assert hd(user_talkies).chats_count == 1
      assert hd(user_talkies).unread_messages_count == 0
    end

    test "does not consider foreign chats and messages for counts" do
      talkie = talkie_fixture()

      chat = chat_fixture(user_fixture(), talkie)
      message_fixture(chat, talkie.user)

      foreign_chat = chat_fixture(user_fixture(), talkie_fixture())
      message_fixture(foreign_chat)

      user_talkies = Convos.list_user_talkies(talkie.user)
      assert hd(user_talkies).chats_count == 1
      assert hd(user_talkies).unread_messages_count == 1
    end

    test "does not consider chats and messages of same users' other talkies for counts" do
      user = user_fixture()
      talkie = talkie_fixture(user)

      chat = chat_fixture(user_fixture(), talkie)
      message_fixture(chat, user)

      other_talkie = talkie_fixture()
      other_talkie_chat = chat_fixture(user, other_talkie)
      message_fixture(other_talkie_chat, other_talkie.user)

      user_talkies = Convos.list_user_talkies(user)
      assert hd(user_talkies).chats_count == 1
      assert hd(user_talkies).unread_messages_count == 1
    end

    test "does not consider chats and messages by other users of sibling talkies for counts" do
      talkie = talkie_fixture()
      sibling_talkie1 = talkie_fixture(user_fixture(), talkie.content)
      sibling_talkie2 = talkie_fixture(user_fixture(), talkie.content)

      chat = chat_fixture(user_fixture(), talkie)
      message_fixture(chat, talkie.user)

      sibling_talkie_chat1 = chat_fixture(user_fixture(), sibling_talkie1)
      message_fixture(sibling_talkie_chat1, sibling_talkie1.user)
      sibling_talkie_chat2 = chat_fixture(user_fixture(), sibling_talkie2)
      message_fixture(sibling_talkie_chat2, sibling_talkie2.user)

      user_talkies = Convos.list_user_talkies(talkie.user)
      assert hd(user_talkies).chats_count == 1
      assert hd(user_talkies).unread_messages_count == 1
    end
  end

  describe "list_sibling_talkies_without_chat/1" do
    test "returns talkies with the same content which do not have a started chat" do
      talkie = talkie_fixture()
      %Talkie{id: sibling_id} = talkie_fixture(user_fixture(), talkie.content)

      talkie_with_chat = talkie_fixture(user_fixture(), talkie.content)
      chat_fixture(talkie.user, talkie_with_chat)

      assert [%Talkie{id: ^sibling_id}] = Convos.list_sibling_talkies_without_chat(talkie)
    end

    test "is not important if other chats are present" do
      talkie = talkie_fixture()
      %Talkie{id: sibling_id} = talkie_fixture(user_fixture(), talkie.content)

      chat_fixture(talkie.user)

      assert [%Talkie{id: ^sibling_id}] = Convos.list_sibling_talkies_without_chat(talkie)
    end

    test "does not return the given talkie" do
      talkie = talkie_fixture()
      assert Convos.list_sibling_talkies_without_chat(talkie) == []
    end

    test "does not return sibling talkies where a chat as requester is present" do
      talkie = talkie_fixture()
      sibling_talkie = talkie_fixture(user_fixture(), talkie.content)

      chat_fixture(talkie.user, sibling_talkie)

      assert Convos.list_sibling_talkies_without_chat(talkie) == []
    end

    test "does not return sibling talkies where a chat as responder is present" do
      talkie = talkie_fixture()
      sibling_talkie = talkie_fixture(user_fixture(), talkie.content)

      chat_fixture(sibling_talkie.user, talkie)

      assert Convos.list_sibling_talkies_without_chat(talkie) == []
    end
  end

  describe "get_talkie!/1" do
    test "returns the talkie with given id" do
      %Talkie{id: id} = talkie_fixture()
      assert %Talkie{id: ^id} = Convos.get_talkie!(id)
    end
  end

  describe "create_talkie/3" do
    test "creates a talkie with valid data" do
      assert %Talkie{} = talkie = talkie_fixture()
      assert talkie.insight_wanted == true
      assert talkie.knowhow_present == true
      assert talkie.knowhow_reason == "some knowhow_reason"
      assert talkie.rating == 42
      assert talkie.reason == "some reason"
      assert talkie.content.url <> "example.com"
    end

    test "validates a rating is present" do
      user = user_fixture()
      {:error, changeset} = Convos.create_talkie(user, content_fixture(user), %{rating: nil})
      assert %{rating: ["can't be blank"]} = errors_on(changeset)
    end
  end

  describe "update_talkie/2" do
    test "updates the talkie with valid data" do
      talkie = talkie_fixture()

      update_attrs = %{
        insight_wanted: false,
        knowhow_present: false,
        knowhow_reason: "some updated knowhow_reason",
        rating: 43,
        reason: "some updated reason",
        url: "https://www.some-updated.url/"
      }

      assert {:ok, %Talkie{} = talkie} = Convos.update_talkie(talkie, update_attrs)
      assert talkie.insight_wanted == false
      assert talkie.knowhow_present == false
      assert talkie.knowhow_reason == "some updated knowhow_reason"
      assert talkie.rating == 43
      assert talkie.reason == "some updated reason"
    end

    test "returns an error changeset with invalid data" do
      talkie = talkie_fixture()
      invalid_attrs = %{rating: nil}

      assert {:error, %Ecto.Changeset{}} = Convos.update_talkie(talkie, invalid_attrs)
      %Talkie{id: id} = talkie
      assert %Talkie{id: ^id} = Convos.get_talkie!(id)
    end
  end

  describe "delete_talkie/1" do
    test "deletes the talkie" do
      talkie = talkie_fixture()
      assert {:ok, %Talkie{}} = Convos.delete_talkie(talkie)
      assert_raise Ecto.NoResultsError, fn -> Convos.get_talkie!(talkie.id) end
    end
  end

  describe "change_talkie/1" do
    test "returns a talkie changeset" do
      talkie = talkie_fixture()
      assert %Ecto.Changeset{} = Convos.change_talkie(talkie)
    end
  end

  describe "change_chat/1" do
    test "returns a chat changeset" do
      chat = chat_fixture()
      assert %Ecto.Changeset{} = Convos.change_chat(chat)
    end
  end

  describe "create_chat/3" do
    test "creates a chat with valid data" do
      requester = user_fixture()
      talkie = talkie_fixture()

      assert %Chat{} = chat = chat_fixture(requester, talkie, %{private: true, banned: true})
      assert chat.requester == requester
      assert chat.responder == talkie.user
      assert chat.content == talkie.content
      assert chat.private == true
      assert chat.banned == true
    end

    test "preloads requester, responder and content" do
      requester = %User{id: requester_id} = user_fixture()
      talkie = %Talkie{user_id: responder_id, content_id: content_id} = talkie_fixture()
      chat = chat_fixture(requester, talkie)

      assert %User{id: ^requester_id} = chat.requester
      assert %User{id: ^responder_id} = chat.responder
      assert %Content{id: ^content_id} = chat.content
    end

    test "it broadcasts a chat created event to the content channel for the requester" do
      talkie = talkie_fixture()
      requester = user_fixture()

      Convos.subscribe_to_content_for_user(talkie.content_id, requester.id)
      chat = chat_fixture(requester, talkie)

      assert_receive {:chat_created, ^chat}
    end

    test "it broadcasts a chat created event to the content channel for the responder" do
      talkie = talkie_fixture()
      responder_id = talkie.user_id

      Convos.subscribe_to_content_for_user(talkie.content_id, responder_id)
      chat = chat_fixture(user_fixture(), talkie)

      assert_receive {:chat_created, ^chat}
    end

    test "it broadcasts a message sent event" do
      talkie = talkie_fixture()
      responder_id = talkie.user_id

      Convos.subscribe_to_content_for_user(talkie.content_id, responder_id)
      chat = chat_fixture(user_fixture(), talkie)
      message = hd(chat.messages) |> Repo.preload(sender: [], chat: [:requester, :responder])

      assert_receive {:chat_created, ^chat}
      assert_receive {:message_sent, ^message}
    end
  end

  describe "get_user_chat!/1" do
    test "returns the chat with given id for participants" do
      %Chat{id: id} = chat = chat_fixture()
      assert %Chat{id: ^id} = Convos.get_user_chat!(id, chat.requester)
      assert %Chat{id: ^id} = Convos.get_user_chat!(id, chat.responder)
    end

    test "does not return the chat with given id for foreign users" do
      assert_raise Ecto.NoResultsError, fn ->
        Convos.get_user_chat!(chat_fixture().id, user_fixture())
      end
    end
  end

  describe "list_chats_for_talkie/1" do
    test "returns chats for the talkie and its siblings" do
      talkie = talkie_fixture()
      sibling_talkie = talkie_fixture(user_fixture(), talkie.content)

      %Chat{id: id} = chat_fixture(user_fixture(), talkie)
      %Chat{id: sibling_id} = chat_fixture(talkie.user, sibling_talkie)

      chat_fixture()
      chat_fixture(talkie.user, talkie_fixture())

      assert [%Chat{id: ^id}, %Chat{id: ^sibling_id}] = Convos.list_chats_for_talkie(talkie)
    end

    test "does not return chats for unrelated talkies" do
      talkie = talkie_fixture()

      chat_fixture()
      chat_fixture(talkie.user, talkie_fixture())

      assert Convos.list_chats_for_talkie(talkie) == []
    end

    test "preloads the latest message of each chat" do
      talkie = talkie_fixture()
      chat = chat_fixture(user_fixture(), talkie)
      sibling_talkie = talkie_fixture(user_fixture(), talkie.content)
      sibling_chat = chat_fixture(talkie.user, sibling_talkie)

      message_fixture(chat)
      %Message{id: id} = message_fixture(chat)

      %Message{id: sibling_id} = message_fixture(sibling_chat)

      assert [
               %Chat{messages: [%Message{id: ^id}]},
               %Chat{messages: [%Message{id: ^sibling_id}]}
             ] = Convos.list_chats_for_talkie(talkie)
    end

    test "adds an unread messages count to each chat" do
      talkie = talkie_fixture()
      chat = chat_fixture(user_fixture(), talkie)
      sibling_talkie = talkie_fixture(user_fixture(), talkie.content)
      sibling_chat = chat_fixture(talkie.user, sibling_talkie)

      message_fixture(chat, talkie.user)
      message_fixture(sibling_chat, talkie.user)

      assert [
               %Chat{unread_messages_count: 1},
               %Chat{unread_messages_count: 0}
             ] = Convos.list_chats_for_talkie(talkie)
    end
  end

  describe "change_message/1" do
    test "returns a message changeset" do
      assert %Ecto.Changeset{} = Convos.change_message(%Message{})
    end
  end

  describe "create_message/1" do
    test "creates a message with valid data" do
      chat = chat_fixture()
      sender = chat.requester

      assert %Message{} = message = message_fixture(chat, sender, text: "some text")
      assert message.text == "some text"
      assert message.chat == chat
      assert message.sender == sender
    end

    test "does not allow sending messages to other chats" do
      chat = chat_fixture()
      some_other_user = user_fixture()

      assert {:error, changeset} =
               Convos.create_message(chat, some_other_user, %{text: "some text"})

      assert {"does not belong to chat", _} = changeset.errors[:sender_id]
    end

    test "it broadcasts a message sent event to the chat channel" do
      chat = chat_fixture()
      receiver = chat.requester
      sender = chat.responder

      Convos.subscribe_to_chat_messages_for(chat.id, receiver.id)
      message = message_fixture(chat, sender)

      assert_receive {:message_sent, ^message}
    end

    test "it broadcasts a message sent event to chat content channel for receiver" do
      chat = chat_fixture()
      receiver = chat.requester
      sender = chat.responder

      Convos.subscribe_to_content_for_user(chat.content_id, receiver.id)
      message = message_fixture(chat, sender)

      assert_receive {:message_sent, ^message}
    end

    test "it broadcasts a message sent event to chat content channel for sender" do
      chat = chat_fixture()
      sender = chat.requester

      Convos.subscribe_to_content_for_user(chat.content_id, sender.id)
      message = message_fixture(chat, sender)

      assert_receive {:message_sent, ^message}
    end
  end

  describe "list_messages_for_chat/1" do
    test "returns messages for the chat" do
      text = "some text"
      chat = chat_fixture(user_fixture(), talkie_fixture(), %{messages: [%{text: text}]})

      assert [%Message{text: ^text}] = Convos.list_messages_for_chat(chat)
    end
  end

  describe "mark_messages_as_read/2" do
    test "sets read_at on messages received by user" do
      chat = chat_fixture()
      message = message_fixture(chat, chat.requester)

      Convos.mark_messages_as_read(chat, chat.responder)

      assert !is_nil(Repo.get!(Message, message.id).read_at)
    end

    test "does not update messages sent by user" do
      chat = chat_fixture()
      message = message_fixture(chat, chat.requester)

      Convos.mark_messages_as_read(chat, chat.requester)

      assert is_nil(Repo.get!(Message, message.id).read_at)
    end

    test "does not update messages of another chat" do
      chat = chat_fixture()
      message = message_fixture()

      Convos.mark_messages_as_read(chat, chat.requester)
      Convos.mark_messages_as_read(chat, chat.responder)

      assert is_nil(Repo.get!(Message, message.id).read_at)
    end
  end
end
