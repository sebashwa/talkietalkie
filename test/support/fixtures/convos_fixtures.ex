defmodule Talkietalkie.ConvosFixtures do
  import Talkietalkie.AccountsFixtures

  alias Talkietalkie.Convos
  alias Talkietalkie.Convos.Content

  @moduledoc """
  This module defines test helpers for creating
  entities via the `Talkietalkie.Convos` context.
  """

  def unique_url, do: "example.com/#{System.unique_integer()}"

  @valid_content_attrs %{
    title: "some title"
  }

  @valid_talkie_attrs %{
    insight_wanted: true,
    knowhow_present: true,
    knowhow_reason: "some knowhow_reason",
    rating: 42,
    reason: "some reason",
    content: %{url: "some.url"}
  }

  @valid_chat_attrs %{
    private: false,
    banned: false
  }

  @valid_message_attrs %{
    text: "some text"
  }

  def content_fixture(user \\ user_fixture(), attrs \\ %{}) do
    content = %Content{user_id: user.id}

    final_attrs =
      attrs
      |> Enum.into(@valid_content_attrs)
      |> Enum.into(%{url: unique_url()})

    struct(content, final_attrs)
  end

  def talkie_fixture(user \\ user_fixture(), content \\ nil, attrs \\ %{}) do
    final_content = content || content_fixture(user)
    final_attrs = Enum.into(attrs, @valid_talkie_attrs)
    {:ok, talkie} = Convos.create_talkie(user, final_content, final_attrs)

    talkie
  end

  def chat_fixture(requester \\ user_fixture(), talkie \\ talkie_fixture(), attrs \\ %{}) do
    first_message_sender_attrs = %{sender_id: requester.id}
    given_first_message_attrs = List.first(attrs[:messages] || [])

    first_message_attrs =
      first_message_sender_attrs
      |> Map.merge(given_first_message_attrs || @valid_message_attrs)

    attrs = Map.merge(attrs, %{messages: [first_message_attrs]})
    final_attrs = Enum.into(attrs, @valid_chat_attrs)
    {:ok, chat} = Convos.create_chat(requester, talkie, final_attrs)

    chat
  end

  def message_fixture(chat \\ chat_fixture(), sender \\ nil, attrs \\ %{}) do
    final_sender = sender || chat.requester
    final_attrs = Enum.into(attrs, @valid_message_attrs)
    {:ok, message} = Convos.create_message(chat, final_sender, final_attrs)

    message
  end
end
