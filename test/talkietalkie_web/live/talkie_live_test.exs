defmodule TalkietalkieWeb.TalkieLiveTest do
  use TalkietalkieWeb.ConnCase

  import Phoenix.LiveViewTest

  import Talkietalkie.ConvosFixtures
  import Talkietalkie.AccountsFixtures

  @create_attrs %{content: %{url: "http://localhost:4002/users/log_in"}}
  @update_attrs %{reason: "New Reason", rating: 5}
  @invalid_attrs %{content: %{url: "htp:/invalid_url"}}

  defp create_user_talkie(%{conn: conn, user: user}) do
    talkie = talkie_fixture(user)
    %{talkie: talkie, user: user, conn: conn}
  end

  setup params do
    params |> register_and_log_in_user |> create_user_talkie
  end

  describe "Talkie index page" do
    test "lists all user talkies", %{conn: conn} do
      {:ok, _talkie_index, html} = live(conn, Routes.talkie_index_path(conn, :index))

      assert html =~ "My talkies"
    end

    test "allows deleting of talkie", %{conn: conn, talkie: talkie} do
      {:ok, talkie_index, _html} = live(conn, Routes.talkie_index_path(conn, :index))

      assert talkie_index |> element("#talkie-#{talkie.id} a", "Delete") |> render_click()
      refute has_element?(talkie_index, "#talkie-#{talkie.id}")
    end
  end

  describe "Talkie show page" do
    test "displays a talkie with its sibling talkies", %{conn: conn, talkie: talkie} do
      sibling_talkie_user_email = "sibling_talkie@example.com"
      sibling_talkie_user = user_fixture(email: sibling_talkie_user_email)
      talkie_fixture(sibling_talkie_user, talkie.content)

      {:ok, _talkie_show, html} = live(conn, Routes.talkie_show_path(conn, :show, talkie))

      assert html =~ talkie.content.title
      assert html =~ sibling_talkie_user_email
    end

    test "displays started chats with one of the sibling talkies", %{conn: conn, talkie: talkie} do
      sibling_talkie = talkie_fixture(user_fixture(), talkie.content)
      chat = chat_fixture(talkie.user, sibling_talkie)

      first_chat_message_text = "This is the text message"
      message_fixture(chat, talkie.user, text: first_chat_message_text)

      {:ok, _talkie_show, html} = live(conn, Routes.talkie_show_path(conn, :show, talkie))

      assert html =~ first_chat_message_text
    end
  end

  describe "Talkie form page" do
    test "fetches content for a url", %{conn: conn} do
      {:ok, talkie_form, _html} = live(conn, Routes.talkie_form_path(conn, :new))

      assert talkie_form
             |> form("#talkie-form", talkie: @create_attrs)
             |> render_change(%{_target: ["talkie", "content", "url"]}) =~ "LOADING..."

      assert render(talkie_form) =~ "talkietalkie - personal dialogues"
    end

    test "allows to create a new talkie after fetching content", %{conn: conn} do
      {:ok, talkie_form, _html} = live(conn, Routes.talkie_form_path(conn, :new))

      valid_form = form(talkie_form, "#talkie-form", talkie: @create_attrs)
      render_change(valid_form, %{_target: ["talkie", "content", "url"]})

      {:ok, _, html} =
        valid_form
        |> render_submit()
        |> follow_redirect(conn, Routes.talkie_index_path(conn, :index))

      assert html =~ "Talkie created successfully"
    end

    test "allows to update a talkie", %{conn: conn, talkie: talkie} do
      {:ok, talkie_form, _html} = live(conn, Routes.talkie_form_path(conn, :edit, talkie))

      {:ok, _, html} =
        talkie_form
        |> form("#talkie-form", talkie: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.talkie_index_path(conn, :index))

      assert html =~ "Talkie updated successfully"
    end

    test "renders an url error message when invalid", %{conn: conn} do
      {:ok, talkie_form, _html} = live(conn, Routes.talkie_form_path(conn, :new))

      html =
        talkie_form
        |> form("#talkie-form", talkie: @invalid_attrs)
        |> render_change(%{_target: ["talkie", "content", "url"]})

      assert html =~ "has to be a URL"
    end
  end
end
