defmodule TalkietalkieWeb.ChatLiveTest do
  use TalkietalkieWeb.ConnCase

  import Phoenix.LiveViewTest

  import Talkietalkie.ConvosFixtures

  setup params do
    register_and_log_in_user(params)
  end

  @create_attrs %{messages: %{"0": %{text: "First chat message text"}}}

  describe "Chat form page" do
    test "allows to create a new chat", %{conn: conn} do
      params = %{talkie_id: talkie_fixture().id}
      {:ok, chat_form, _html} = live(conn, Routes.chat_form_path(conn, :new, params))

      submitted_form = form(chat_form, "#chat-form", chat: @create_attrs) |> render_submit()

      {_, {:live_redirect, %{to: show_chat_path}}} = submitted_form
      {:ok, _chat_show, html} = follow_redirect(submitted_form, conn, show_chat_path)

      assert html =~ "First chat message text"
    end

    test "allows to send messages to an existing chat", %{conn: conn, user: user} do
      chat = chat_fixture(user)
      {:ok, chat_form, _html} = live(conn, Routes.chat_form_path(conn, :edit, chat))

      html =
        chat_form
        |> form("#message-form", message: %{text: "New Message"})
        |> render_submit()

      assert html =~ "New Message"
    end

    test "it is now allowed to edit foreign chats", %{conn: conn} do
      chat = chat_fixture()

      assert_raise Ecto.NoResultsError, fn ->
        live(conn, Routes.chat_form_path(conn, :edit, chat))
      end
    end
  end
end
