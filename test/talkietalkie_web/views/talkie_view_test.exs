defmodule TalkietalkieWeb.TalkieViewTest do
  use TalkietalkieWeb.ConnCase, async: true

  alias Talkietalkie.Convos.Talkie
  alias TalkietalkieWeb.TalkieView
  alias Ecto.Changeset

  describe "knowhow_present?/2" do
    test "is present if the checkbox is ticked" do
      changeset = %Changeset{changes: %{knowhow_present: true}}
      assert TalkieView.knowhow_present?(changeset, %Talkie{}) == true
    end

    test "is present if set on existent talkie" do
      talkie = %Talkie{knowhow_present: true}
      assert TalkieView.knowhow_present?(%Changeset{}, talkie) == true
    end

    test "is not present if checkbox is unticked" do
      talkie = %Talkie{knowhow_present: true}
      changeset = %Changeset{changes: %{knowhow_present: false}}
      assert TalkieView.knowhow_present?(changeset, talkie) == false
    end
  end

  describe "get_current_rating/2" do
    test "uses rating from the changes" do
      changeset = %Changeset{changes: %{rating: 9001}}
      talkie = %Talkie{rating: 1}
      assert TalkieView.get_current_rating(changeset, talkie) == 9001
    end

    test "uses rating of existing talkie" do
      changeset = %Changeset{}
      talkie = %Talkie{rating: 9002}
      assert TalkieView.get_current_rating(changeset, talkie) == 9002
    end

    test "uses the default rating" do
      assert TalkieView.get_current_rating(%Changeset{}, %Talkie{}) == 5
    end
  end

  describe "content_possible?/2" do
    test "it is possible if url is valid and not currrently fetching" do
      changeset = %Changeset{changes: %{content: %Changeset{errors: nil}}}
      fetching_content = false
      assert TalkieView.content_possible?(changeset, fetching_content) == true
    end

    test "it is not possible if url is invalid" do
      errors = %{url: %{"some" => "error"}}
      changeset = %Changeset{changes: %{content: %Changeset{errors: errors}}}
      fetching_content = false
      assert TalkieView.content_possible?(changeset, fetching_content) == false
    end

    test "it is not possible if currently fetching" do
      changeset = %Changeset{changes: %{content: %Changeset{errors: nil}}}
      fetching_content = true
      assert TalkieView.content_possible?(changeset, fetching_content) == false
    end
  end
end
