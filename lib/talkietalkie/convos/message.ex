defmodule Talkietalkie.Convos.Message do
  @moduledoc """
  Message is a chat message that belongs to a chat.
  """

  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Talkietalkie.Accounts.User
  alias Talkietalkie.Convos.Chat

  @type t :: %__MODULE__{
          text: String.t() | nil,
          read_at: NaiveDateTime.t() | nil,
          sender: User.t() | Ecto.Association.NotLoaded.t(),
          chat: Chat.t() | Ecto.Association.NotLoaded.t()
        }

  @type changeset_t :: Ecto.Changeset.t(__MODULE__.t())

  schema "messages" do
    field :text, :string
    field :read_at, :naive_datetime
    belongs_to :sender, User
    belongs_to :chat, Chat

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:text, :sender_id])
    |> validate_required([:text])
    |> foreign_key_constraint(:sender_id)
    |> foreign_key_constraint(:sender_id,
      message: "does not belong to chat",
      name: "message_sender_does_not_belong_to_chat"
    )
    |> foreign_key_constraint(:chat_id)
    |> validate_length(:text, min: 1)
  end

  def for_chat(query, %Chat{id: chat_id}) do
    from m in query, where: m.chat_id == ^chat_id
  end

  def received_by(query, %User{id: user_id}) do
    from m in query, where: m.sender_id != ^user_id
  end

  def unread(query) do
    from m in query, where: is_nil(m.read_at)
  end
end
