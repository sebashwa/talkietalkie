defmodule Talkietalkie.Convos.Talkie do
  @moduledoc """
  Talkie expresses an interest to talk about something.
  It is always related to a content and belongs to a user.
  Other talkies for the same content are called siblings.
  """

  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Talkietalkie.Accounts.User
  alias Talkietalkie.Convos.{Content, Chat, Message}

  @type t :: %__MODULE__{
          insight_wanted: boolean,
          knowhow_present: boolean,
          knowhow_reason: String.t() | nil,
          rating: integer | nil,
          reason: String.t() | nil,
          siblings_count: integer | nil,
          chats_count: integer | nil,
          unread_messages_count: integer | nil,
          content: Content.t() | Ecto.Association.NotLoaded.t(),
          user: User.t() | Ecto.Association.NotLoaded.t()
        }

  @type changeset_t :: Ecto.Changeset.t(__MODULE__.t())

  schema "talkies" do
    field :insight_wanted, :boolean, default: false
    field :knowhow_present, :boolean, default: false
    field :knowhow_reason, :string
    field :rating, :integer
    field :reason, :string
    field :siblings_count, :integer, virtual: true
    field :chats_count, :integer, virtual: true
    field :unread_messages_count, :integer, virtual: true
    belongs_to :content, Content
    belongs_to :user, User

    timestamps()
  end

  @spec changeset(__MODULE__.t(), map) :: __MODULE__.changeset_t()
  def changeset(talkie, attrs) do
    talkie
    |> cast(attrs, [
      :insight_wanted,
      :knowhow_present,
      :knowhow_reason,
      :rating,
      :reason
    ])
    |> cast_assoc(:content)
    |> validate_required([:rating])
    |> foreign_key_constraint(:content_id)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:unique_content_for_user, name: :unique_content_for_user)
  end

  def for_user(query, %User{id: user_id}) do
    from t in query, where: t.user_id == ^user_id, preload: [:content]
  end

  def for_content(query, %Content{id: content_id}) do
    from t in query, where: t.content_id == ^content_id, preload: [:content]
  end

  def not_talkie(query, %__MODULE__{id: talkie_id}) do
    from t in query, where: t.id != ^talkie_id
  end

  def add_siblings_count(query) do
    from t in query,
      left_join: s in __MODULE__,
      on: s.id != t.id and s.content_id == t.content_id,
      group_by: t.id,
      select_merge: %{siblings_count: count(s.id, :distinct)}
  end

  def add_chats_count(query) do
    from [t, chats: c] in query,
      group_by: t.id,
      select_merge: %{chats_count: count(c.id, :distinct)}
  end

  def add_unread_messages_counts(query) do
    from [t, chats: c] in query,
      left_join: m in Message,
      on: m.chat_id == c.id and m.sender_id != t.user_id and is_nil(m.read_at),
      group_by: t.id,
      select_merge: %{unread_messages_count: count(m.id, :distinct)}
  end

  def order_by_latest_message_inserted_at(query) do
    from [t, chats: c] in query,
      left_join: m in Message,
      on: m.chat_id == c.id,
      group_by: t.id,
      order_by: [desc_nulls_last: max(m.inserted_at)]
  end

  def not_having_chat_with(query, %User{id: user_id}) do
    from t in query,
      left_join: c in Chat,
      on:
        c.content_id == t.content_id and
          ((c.responder_id == ^user_id and c.requester_id == t.user_id) or
             (c.responder_id == t.user_id and c.requester_id == ^user_id)),
      where: is_nil(c)
  end

  def join_chats(query) do
    from t in query,
      left_join: c in Chat,
      as: :chats,
      on:
        c.content_id == t.content_id and
          (c.responder_id == t.user_id or c.requester_id == t.user_id)
  end
end
