defmodule Talkietalkie.Convos.Chat do
  @moduledoc """
  Chat holds all meta information about a chat
  between two parties.
  It is always related to a content.
  """

  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Talkietalkie.Convos.{Content, Message}
  alias Talkietalkie.Accounts.User

  @type t :: %__MODULE__{
          banned: boolean,
          private: boolean,
          unread_messages_count: integer | nil,
          requester: User.t() | Ecto.Association.NotLoaded.t(),
          responder: User.t() | Ecto.Association.NotLoaded.t(),
          content: Content.t() | Ecto.Association.NotLoaded.t(),
          messages: [Message.t()] | nil
        }

  @type changeset_t :: Ecto.Changeset.t(__MODULE__.t())

  schema "chats" do
    field :banned, :boolean, default: false
    field :private, :boolean, default: false
    field :unread_messages_count, :integer, virtual: true
    belongs_to :requester, User
    belongs_to :responder, User
    belongs_to :content, Content
    has_many :messages, Message

    timestamps()
  end

  @doc false
  def changeset(chat, attrs) do
    chat
    |> cast(attrs, [:banned, :private])
    |> cast_assoc(:messages, required: true)
    |> foreign_key_constraint(:content_id)
    |> foreign_key_constraint(:responder_id)
    |> foreign_key_constraint(:requester_id)
    |> unique_constraint(:unique_content_for_users, name: :unique_content_for_users)
    |> check_constraint(:requester, name: :requester_not_responder)
  end

  def for_participant(query, %User{id: id}) do
    from c in query, where: c.responder_id == ^id or c.requester_id == ^id
  end

  def for_content(query, %Content{id: id}) do
    from c in query, where: c.content_id == ^id
  end

  def order_by_latest_message_inserted_at(query) do
    from c in query,
      left_join: m in Message,
      on: m.chat_id == c.id,
      group_by: c.id,
      order_by: [desc: max(m.inserted_at)]
  end

  def add_unread_messages_count(query, %User{id: user_id}) do
    from c in query,
      left_join: m in Message,
      on: m.chat_id == c.id and m.sender_id != ^user_id and is_nil(m.read_at),
      group_by: c.id,
      select_merge: %{unread_messages_count: count(m.id, :distinct)}
  end

  def with_latest_message(query) do
    latest_message = from m in Message, distinct: m.chat_id, order_by: [desc: m.id]
    from query, preload: [messages: ^latest_message]
  end

  def partner_for(chat, user) do
    if user.id == chat.requester.id do
      chat.responder
    else
      chat.requester
    end
  end
end
