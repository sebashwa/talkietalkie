defmodule Talkietalkie.Convos.Content do
  @moduledoc """
  Content holds information about what people might be interested in.
  So far it is only possible to talk about content behind urls on the web.
  """

  use Ecto.Schema
  import Ecto.Changeset
  import TalkietalkieWeb.Gettext

  alias Talkietalkie.Accounts.User
  alias Talkietalkie.Convos.Talkie

  @type t :: %__MODULE__{
          id: integer,
          url: String.t(),
          title: String.t() | nil,
          user: User.t(),
          talkies: [Talkie.t()],
          inserted_at: NaiveDateTime.t(),
          updated_at: NaiveDateTime.t()
        }

  schema "contents" do
    field :url, :string
    field :title, :string
    belongs_to :user, User
    has_many :talkies, Talkie

    timestamps()
  end

  @protocol_www "(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)"

  @doc false
  def changeset(content, attrs) do
    url_regex =
      ~r/^#{@protocol_www}?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*(\.[a-z]{2,})?(:[0-9]{1,5})?(\/.*)?$/

    content
    |> cast(attrs, [:title, :url, :user_id])
    |> validate_required([:url])
    |> validate_format(:url, url_regex, message: gettext("has to be a URL"))
    |> foreign_key_constraint(:user_id)
    |> strip_url()
  end

  @doc """
  Strips protocol, www-subdomain and trailing slashes to make sure
  urls are as uniform as possible
  """
  def strip_url(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{url: url}} ->
        stripped_url = String.replace(url, ~r/^#{@protocol_www}/, "")
        put_change(changeset, :url, stripped_url)

      _ ->
        changeset
    end
  end
end
