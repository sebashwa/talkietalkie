defmodule Talkietalkie.Repo do
  use Ecto.Repo,
    otp_app: :talkietalkie,
    adapter: Ecto.Adapters.Postgres
end
