defmodule Talkietalkie.Convos do
  @moduledoc """
  Convos handles everything about conversations:
  * Content to talk about
  * User interest about content (talkie)
  * Chats as conversations about content
  * Messages of chats
  """

  import Ecto.Query, warn: false
  alias Talkietalkie.Repo

  alias Talkietalkie.Accounts.User
  alias Talkietalkie.Convos.{Content, Talkie, Chat, Message}

  # Content
  # =======

  @spec build_content_from_link_preview(String.t(), User.t()) :: Content.t() | nil
  def build_content_from_link_preview(url, %User{} = user) do
    case LinkPreview.create(url) do
      {:ok, preview} -> %Content{url: url, title: preview.title, user_id: user.id}
      {:error, _} -> nil
    end
  end

  @spec get_content_by_url(String.t()) :: Content.t() | nil
  def get_content_by_url(url) do
    Repo.get_by(Content, url: url)
  end

  # Talkie
  # ======

  @spec change_talkie(Talkie.t(), map()) :: Talkie.changeset_t()
  def change_talkie(%Talkie{} = talkie, attrs \\ %{}) do
    Talkie.changeset(talkie, attrs)
  end

  @spec create_talkie(User.t(), Content.t(), map()) ::
          {:ok, Talkie.t()} | {:error, Talkie.changeset_t()}
  def create_talkie(%User{} = user, %Content{} = content, attrs \\ %{}) do
    %Talkie{}
    |> Talkie.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:user, user)
    |> Ecto.Changeset.put_assoc(:content, content)
    |> Repo.insert()
  end

  @spec get_talkie!(integer()) :: Talkie.t()
  def get_talkie!(id) do
    Talkie
    |> Repo.get!(id)
    |> Repo.preload([:content, :user])
  end

  @spec get_user_talkie!(User.t(), integer()) :: Talkie.t()
  def get_user_talkie!(%User{} = user, id) do
    Talkie
    |> Talkie.for_user(user)
    |> Repo.get!(id)
    |> Repo.preload([:content, :user])
  end

  @spec list_user_talkies(User.t()) :: [Talkie.t()]
  def list_user_talkies(%User{} = user) do
    Talkie
    |> Talkie.for_user(user)
    |> Talkie.add_siblings_count()
    |> Talkie.join_chats()
    |> Talkie.add_chats_count()
    |> Talkie.add_unread_messages_counts()
    |> Talkie.order_by_latest_message_inserted_at()
    |> Repo.all()
  end

  @spec list_sibling_talkies_without_chat(Talkie.t()) :: [Talkie.t()]
  def list_sibling_talkies_without_chat(%Talkie{} = talkie) do
    Talkie
    |> Talkie.for_content(talkie.content)
    |> Talkie.not_talkie(talkie)
    |> Talkie.not_having_chat_with(talkie.user)
    |> Repo.all()
    |> Repo.preload(:user)
  end

  @spec update_talkie(Talkie.t(), map()) :: {:ok, Talkie.t()} | {:error, Talkie.changeset_t()}
  def update_talkie(%Talkie{} = talkie, attrs) do
    talkie
    |> Talkie.changeset(attrs)
    |> Repo.update()
  end

  @spec delete_talkie(Talkie.t()) :: {:ok, Talkie.t()} | {:error, Talkie.changeset_t()}
  def delete_talkie(%Talkie{} = talkie) do
    Repo.delete(talkie)
  end

  # Chat
  # ====

  @spec change_chat(Chat.t(), map()) :: Chat.changeset_t()
  def change_chat(%Chat{} = chat, attrs \\ %{}) do
    Chat.changeset(chat, attrs)
  end

  @spec create_chat(User.t(), Talkie.t(), map()) :: {:ok, Chat.t()} | {:error, Chat.changeset_t()}
  def create_chat(%User{} = requester, %Talkie{} = talkie, attrs \\ %{}) do
    %Chat{}
    |> Chat.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:requester, requester)
    |> Ecto.Changeset.put_assoc(:responder, talkie.user)
    |> Ecto.Changeset.put_assoc(:content, talkie.content)
    |> Repo.insert()
    |> broadcast_created(chat_created_event())
  end

  @spec get_user_chat!(integer(), User.t()) :: Chat.t()
  def get_user_chat!(id, %User{} = user) do
    Chat
    |> Chat.for_participant(user)
    |> Repo.get!(id)
    |> Repo.preload([:requester, :responder, :content])
  end

  @spec list_chats_for_talkie(Talkie.t()) :: [Chat.t()]
  def list_chats_for_talkie(%Talkie{} = talkie) do
    Chat
    |> Chat.for_content(talkie.content)
    |> Chat.for_participant(talkie.user)
    |> Chat.with_latest_message()
    |> Chat.add_unread_messages_count(talkie.user)
    |> Chat.order_by_latest_message_inserted_at()
    |> Repo.all()
    |> Repo.preload([:requester, :responder])
  end

  # Message
  # =======

  @spec change_message(Message.t(), map()) :: Message.changeset_t()
  def change_message(%Message{} = message, attrs \\ %{}) do
    Message.changeset(message, attrs)
  end

  @spec create_message(Chat.t(), User.t(), map()) ::
          {:ok, Message.t()} | {:error, Message.changeset_t()}
  def create_message(%Chat{} = chat, %User{} = sender, attrs \\ %{}) do
    %Message{}
    |> Message.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:sender, sender)
    |> Ecto.Changeset.put_assoc(:chat, chat)
    |> Repo.insert()
    |> broadcast_created(message_sent_event())
  end

  @spec list_messages_for_chat(Chat.t()) :: [Message.t()]
  def list_messages_for_chat(%Chat{} = chat) do
    Message
    |> Message.for_chat(chat)
    |> Repo.all()
  end

  @spec mark_messages_as_read(Chat.t(), User.t()) :: {integer(), nil | [term()]}
  def mark_messages_as_read(%Chat{} = chat, %User{} = user) do
    Message
    |> Message.for_chat(chat)
    |> Message.unread()
    |> Message.received_by(user)
    |> Repo.update_all(set: [read_at: NaiveDateTime.utc_now()])
  end

  def subscribe_to_chat_messages_for(chat_id, user_id) do
    Phoenix.PubSub.subscribe(
      Talkietalkie.PubSub,
      chat_messages_for_user_channel(chat_id, user_id)
    )
  end

  def subscribe_to_messages_from_and_by(user_id) do
    Phoenix.PubSub.subscribe(Talkietalkie.PubSub, messages_for_user_channel(user_id))
    Phoenix.PubSub.subscribe(Talkietalkie.PubSub, messages_by_user_channel(user_id))
  end

  def subscribe_to_content_for_user(content_id, user_id) do
    Phoenix.PubSub.subscribe(Talkietalkie.PubSub, content_user_channel(content_id, user_id))
  end

  defp broadcast_created({:error, _reason} = error, _event), do: error

  defp broadcast_created({:ok, %Chat{} = chat}, event) do
    content_requester_channel = content_user_channel(chat.content_id, chat.requester_id)
    content_responder_channel = content_user_channel(chat.content_id, chat.responder_id)

    Phoenix.PubSub.broadcast(Talkietalkie.PubSub, content_requester_channel, {event, chat})
    Phoenix.PubSub.broadcast(Talkietalkie.PubSub, content_responder_channel, {event, chat})

    message = hd(chat.messages) |> Repo.preload(sender: [], chat: [:requester, :responder])
    broadcast_created({:ok, message}, message_sent_event())

    {:ok, chat}
  end

  defp broadcast_created({:ok, %Message{} = message}, event) do
    chat = message.chat
    sender = message.sender
    receiver = Chat.partner_for(chat, sender)

    payload = {event, message}
    content_sender_channel = content_user_channel(chat.content_id, sender.id)
    content_receiver_channel = content_user_channel(chat.content_id, receiver.id)
    chat_messages_for_receiver_channel = chat_messages_for_user_channel(chat.id, receiver.id)

    Phoenix.PubSub.broadcast(Talkietalkie.PubSub, chat_messages_for_receiver_channel, payload)
    Phoenix.PubSub.broadcast(Talkietalkie.PubSub, content_receiver_channel, payload)
    Phoenix.PubSub.broadcast(Talkietalkie.PubSub, content_sender_channel, payload)
    Phoenix.PubSub.broadcast(Talkietalkie.PubSub, messages_for_user_channel(receiver.id), payload)
    Phoenix.PubSub.broadcast(Talkietalkie.PubSub, messages_by_user_channel(sender.id), payload)

    {:ok, message}
  end

  defp chat_messages_for_user_channel(chat_id, user_id),
    do: "chat-#{chat_id}-messages-for-#{user_id}"

  defp content_user_channel(content_id, user_id), do: "content-#{content_id}-for-user-#{user_id}"
  defp messages_for_user_channel(user_id), do: "messages-for-user-#{user_id}"
  defp messages_by_user_channel(user_id), do: "messages-by-user-#{user_id}"
  defp message_sent_event, do: :message_sent
  defp chat_created_event, do: :chat_created
end
