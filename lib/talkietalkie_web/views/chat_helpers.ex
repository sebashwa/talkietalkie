defmodule TalkietalkieWeb.ChatHelpers do
  @moduledoc """
  Convenience functions to work with chats
  """
  use Phoenix.HTML

  alias Talkietalkie.Convos.Chat

  def partner_for(chat, user) do
    Chat.partner_for(chat, user)
  end
end
