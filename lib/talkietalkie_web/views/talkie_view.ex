defmodule TalkietalkieWeb.TalkieView do
  use TalkietalkieWeb, :view

  import TalkietalkieWeb.ChatHelpers

  @default_rating 5

  def host_or_path(url_string) do
    uri = URI.parse(url_string)
    uri.host || uri.path
  end

  def knowhow_present?(changeset, talkie) do
    changes = changeset.changes

    changes[:knowhow_present] ||
      (talkie.knowhow_present && !(changes[:knowhow_present] == false))
  end

  def get_current_rating(changeset, talkie) do
    changeset.changes[:rating] || talkie.rating || @default_rating
  end

  def content_possible?(changeset, fetching_content) do
    content_changeset = changeset.changes[:content]
    valid_url = content_changeset && !content_changeset.errors[:url]
    valid_url && !fetching_content
  end
end
