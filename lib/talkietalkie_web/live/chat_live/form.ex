defmodule TalkietalkieWeb.ChatLive.Form do
  @moduledoc """
  ChatLive.Form is the form to create a chat.
  A chat is always created with a message attached.
  """

  use TalkietalkieWeb, :live_view

  alias Talkietalkie.Convos
  alias Talkietalkie.Convos.{Chat, Message}

  @impl true
  def mount(_params, session, socket) do
    {:ok, assign_defaults(socket, session), temporary_assigns: [messages: []]}
  end

  @impl true
  def render(assigns) do
    TalkietalkieWeb.ChatView.render("form.html", assigns)
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  @impl true
  def handle_event("change", unsigned_params, socket) do
    {:noreply, validate_form(socket, unsigned_params)}
  end

  @impl true
  def handle_event("write", %{"message" => %{"text" => text}}, socket) do
    {:noreply, assign(socket, :text, text)}
  end

  @impl true
  def handle_event("save", %{"chat" => chat_params}, socket) do
    save_chat(socket, socket.assigns.live_action, chat_params)
  end

  @impl true
  def handle_event("send", %{"message" => message_params}, socket) do
    %{current_user: sender, chat: chat} = socket.assigns

    case Convos.create_message(chat, sender, message_params) do
      {:ok, message} ->
        {:noreply,
         socket
         |> assign(:text, "")
         |> update(:messages, fn messages -> [message | messages] end)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply,
         socket
         |> put_flash(:error, "Error while sending message")
         |> assign(changeset: changeset)}
    end
  end

  @impl true
  def handle_info({:message_sent, message}, socket) do
    %{current_user: current_user, chat: chat} = socket.assigns
    Convos.mark_messages_as_read(chat, current_user)
    {:noreply, update(socket, :messages, fn messages -> [message | messages] end)}
  end

  defp apply_action(socket, :new, %{"talkie_id" => talkie_id}) do
    talkie = Convos.get_talkie!(talkie_id)
    chat = %Chat{messages: [%Message{}]}

    socket
    |> assign(:page_title, "New Chat")
    |> assign(:chat, chat)
    |> assign(:talkie, talkie)
    |> assign(:changeset, Convos.change_chat(chat))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> put_flash(:info, "No talkie given")
    |> push_redirect(to: Routes.talkie_index_path(socket, :index))
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    %{current_user: current_user} = socket.assigns
    chat = Convos.get_user_chat!(id, current_user)
    messages = Convos.list_messages_for_chat(chat)
    message_changeset = Convos.change_message(%Message{})

    Convos.mark_messages_as_read(chat, current_user)

    if connected?(socket) do
      Convos.subscribe_to_chat_messages_for(id, current_user.id)
    end

    socket
    |> assign(:chat, chat)
    |> assign(:messages, messages)
    |> assign(:text, "")
    |> assign(:message_changeset, message_changeset)
  end

  defp validate_form(socket, %{"chat" => chat_params}) do
    changeset =
      socket.assigns.chat
      |> Convos.change_chat(chat_params)
      |> Map.put(:action, :validate)

    assign(socket, :changeset, changeset)
  end

  defp save_chat(socket, :new, chat_params) do
    %{current_user: requester, talkie: talkie} = socket.assigns

    case Convos.create_chat(requester, talkie, chat_params) do
      {:ok, chat} ->
        {:noreply, push_redirect(socket, to: Routes.chat_form_path(socket, :edit, chat))}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply,
         socket
         |> assign(changeset: changeset)
         |> put_flash(:error, "Something went wrong")}
    end
  end
end
