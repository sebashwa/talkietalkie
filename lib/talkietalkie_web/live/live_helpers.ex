defmodule TalkietalkieWeb.LiveHelpers do
  @moduledoc """
  LiveHelpers holds helper functions common to multiple live views.
  """

  import Phoenix.LiveView

  alias Talkietalkie.Accounts
  alias TalkietalkieWeb.Router.Helpers, as: Routes

  @doc """
  Is used to set values on the socket, which are often used.
  For not only the currentuser is assigned.
  """
  def assign_defaults(socket, session) do
    get_current_user = fn -> Accounts.get_user_by_session_token(session["user_token"]) end
    socket = assign_new(socket, :current_user, get_current_user)

    if socket.assigns.current_user do
      socket
    else
      redirect(socket, to: Routes.user_session_path(socket, :new))
    end
  end
end
