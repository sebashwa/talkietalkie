defmodule TalkietalkieWeb.TalkieLive.Index do
  @moduledoc """
  TalkieLive.Index lists all talkies of the logged in user.
  A new talkie can be created from this page.
  Existing talkies can be edited, deleted and shown.
  """

  use TalkietalkieWeb, :live_view

  alias Talkietalkie.Convos

  @impl true
  def mount(_params, session, socket) do
    socket = assign_defaults(socket, session)

    if connected?(socket) do
      Convos.subscribe_to_messages_from_and_by(current_user(socket).id)
    end

    {:ok,
     socket
     |> assign(:talkies, list_talkies(socket.assigns.current_user))
     |> assign(:page_title, "My talkies")}
  end

  @impl true
  def render(assigns) do
    TalkietalkieWeb.TalkieView.render("index.html", assigns)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    talkie = Convos.get_user_talkie!(socket.assigns.current_user, id)
    {:ok, _} = Convos.delete_talkie(talkie)

    {:noreply, assign(socket, :talkies, list_talkies(socket.assigns.current_user))}
  end

  @impl true
  def handle_info({:message_sent, _message}, socket) do
    talkies = socket |> current_user |> list_talkies
    {:noreply, assign(socket, :talkies, talkies)}
  end

  defp list_talkies(current_user) do
    Convos.list_user_talkies(current_user)
  end

  defp current_user(socket) do
    socket.assigns.current_user
  end
end
