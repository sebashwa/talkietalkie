defmodule TalkietalkieWeb.TalkieLive.Form do
  @moduledoc """
  TalkieLive.Form is the form to create or edit a talkie.
  A talkie requires a content.
  Once created, the content is no longer editable.
  """

  use TalkietalkieWeb, :live_view

  alias Talkietalkie.Convos
  alias Talkietalkie.Convos.Talkie

  @impl true
  def mount(_params, session, socket) do
    {:ok,
     socket
     |> assign_defaults(session)
     |> assign(:fetching_content, false)
     |> assign(:current_content, nil)}
  end

  @impl true
  def render(assigns) do
    TalkietalkieWeb.TalkieView.render("form.html", assigns)
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  @impl true
  def handle_event("change", unsigned_params, socket) do
    {:noreply,
     socket
     |> validate_form(unsigned_params)
     |> get_content_for_url(unsigned_params)}
  end

  @impl true
  def handle_event("save", %{"talkie" => talkie_params}, socket) do
    save_talkie(socket, socket.assigns.live_action, talkie_params)
  end

  @impl true
  def handle_info({:fetch_content_for_url, url}, socket) do
    current_user = socket.assigns.current_user

    content_match = load_content(url) || Convos.build_content_from_link_preview(url, current_user)

    {:noreply,
     socket
     |> assign(:fetching_content, false)
     |> assign(:current_content, content_match)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    current_user = socket.assigns.current_user
    talkie = Convos.get_user_talkie!(current_user, id)

    socket
    |> assign(:page_title, "Edit talkie")
    |> assign(:talkie, talkie)
    |> assign(:current_content, talkie.content)
    |> assign(:changeset, Convos.change_talkie(talkie))
  end

  defp apply_action(socket, :new, _params) do
    talkie = %Talkie{}

    socket
    |> assign(:page_title, "New talkie")
    |> assign(:talkie, talkie)
    |> assign(:current_content, nil)
    |> assign(:changeset, Convos.change_talkie(talkie))
  end

  defp validate_form(socket, %{"talkie" => talkie_params}) do
    changeset =
      socket.assigns.talkie
      |> Convos.change_talkie(talkie_params)
      |> Map.put(:action, :validate)

    assign(socket, :changeset, changeset)
  end

  defp get_content_for_url(socket, %{"talkie" => talkie_params, "_target" => target}) do
    content_changeset = socket.assigns.changeset.changes[:content]
    url_errors = content_changeset && content_changeset.errors[:url]

    if is_nil(url_errors) && target == ["talkie", "content", "url"] do
      send(self(), {:fetch_content_for_url, talkie_params["content"]["url"]})
      assign(socket, :fetching_content, true)
    else
      socket
    end
  end

  defp save_talkie(socket, :edit, talkie_params) do
    case Convos.update_talkie(socket.assigns.talkie, talkie_params) do
      {:ok, _talkie} ->
        {:noreply,
         socket
         |> put_flash(:info, "Talkie updated successfully")
         |> push_redirect(to: Routes.talkie_index_path(socket, :index))}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_talkie(socket, :new, talkie_params) do
    %{current_user: current_user, current_content: current_content} = socket.assigns

    refetched_content = load_content(current_content.url) || current_content

    case Convos.create_talkie(current_user, refetched_content, talkie_params) do
      {:ok, _talkie} ->
        {:noreply,
         socket
         |> put_flash(:info, "Talkie created successfully")
         |> push_redirect(to: Routes.talkie_index_path(socket, :index))}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  defp load_content(url) do
    Convos.get_content_by_url(url)
  end
end
