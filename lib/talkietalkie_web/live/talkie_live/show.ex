defmodule TalkietalkieWeb.TalkieLive.Show do
  @moduledoc """
  TalkieLive.Show is the detail page for a talkie.
  It shows the own talkie and sibling talkies.
  From here, a chat can be started with the creator of a sibling talkie.
  """

  use TalkietalkieWeb, :live_view

  alias Talkietalkie.Convos

  @impl true
  def mount(_params, session, socket) do
    {:ok,
     socket
     |> assign_defaults(session)
     |> assign(:page_title, "Showing talkie")}
  end

  @impl true
  def render(assigns) do
    TalkietalkieWeb.TalkieView.render("show.html", assigns)
  end

  @impl true
  def handle_params(%{"id" => id}, _url, socket) do
    %{current_user: current_user} = socket.assigns
    talkie = Convos.get_user_talkie!(current_user, id)

    if connected?(socket) do
      Convos.subscribe_to_content_for_user(talkie.content_id, current_user.id)
    end

    {:noreply,
     socket
     |> assign(:talkie, talkie)
     |> assign(:chats, load_chats(talkie))
     |> assign(:siblings, load_siblings(talkie))}
  end

  @impl true
  def handle_info({:message_sent, _message}, socket) do
    talkie = socket.assigns.talkie
    {:noreply, assign(socket, :chats, load_chats(talkie))}
  end

  @impl true
  def handle_info({:chat_created, _message}, socket) do
    talkie = socket.assigns.talkie
    {:noreply, assign(socket, :siblings, load_siblings(talkie))}
  end

  defp load_chats(talkie) do
    Convos.list_chats_for_talkie(talkie)
  end

  defp load_siblings(talkie) do
    Convos.list_sibling_talkies_without_chat(talkie)
  end
end
