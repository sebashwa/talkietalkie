defmodule LinkPreview.Error do
  defexception message: "", origin: nil

  @type t :: %__MODULE__{
          origin: module,
          message: String.t()
        }
end
