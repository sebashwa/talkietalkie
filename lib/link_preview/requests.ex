defmodule LinkPreview.Requests do
  @moduledoc """
    Module providing functions to handle needed requests.
  """
  use Tesla, docs: false, only: ~w(get head)a

  adapter(Tesla.Adapter.Hackney)

  plug Tesla.Middleware.Headers, [
    {"user-agent",
     "Mozilla/5.0 (compatible; DuckDuckBot-Https/1.1; https://duckduckgo.com/duckduckbot)"}
  ]

  plug Tesla.Middleware.FollowRedirects

  @doc """
    Check if given url leads to image.
  """
  @spec image?(String.t()) :: boolean
  def image?(url) do
    case head(url) do
      {:ok, %Tesla.Env{status: 200, headers: headers}} ->
        String.match?(headers["content-type"], ~r/\Aimage\//)

      {:ok, %Tesla.Env{}} ->
        false

      {:error, %Tesla.Error{}} ->
        false
    end
  end
end
