defmodule LinkPreview.Parsers.Html do
  @moduledoc """
    Parser implementation based on html tags.
  """
  alias LinkPreview.Page

  use LinkPreview.Parsers.Basic

  @doc """
    Get page title based on first encountered title tag.

    Config options:
    * `:friendly_strings`\n
      see `LinkPreview.Parsers.Basic.maybe_friendly_string/1` function\n
      default: true
  """
  def title(page, body) do
    title =
      body
      |> Floki.parse_document!()
      |> Floki.find("title")
      |> List.first()
      |> get_text

    %Page{page | title: title}
  end

  @doc """
    Get page description based on first encountered h1..h6 tag.

    Preference: h1> h2 > h3 > h4 > h5 > h6

    Config options:
    * `:friendly_strings`\n
      see `LinkPreview.Parsers.Basic.maybe_friendly_string/1` function\n
      default: true
  """
  def description(page, body) do
    description = search_h(body, 1)

    %Page{page | description: description}
  end

  defp get_text(nil), do: nil

  defp get_text(choosen) do
    choosen |> Floki.text() |> maybe_friendly_string()
  end

  defp search_h(_body, 7), do: nil

  defp search_h(body, level) do
    description =
      body
      |> Floki.parse_document!()
      |> Floki.find("h#{level}")
      |> List.first()
      |> get_text

    case description do
      nil -> search_h(body, level + 1)
      "" -> search_h(body, level + 1)
      _ -> description
    end
  end
end
