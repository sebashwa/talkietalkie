defmodule LinkPreview.Processor do
  @moduledoc """
    Combines the logic of other modules with user input.
  """
  alias LinkPreview.{Page, Requests}
  alias LinkPreview.Error, as: LPError
  alias LinkPreview.Parsers.{Basic, Opengraph, Html, Image}

  @doc """
    Takes url and returns result of processing.
  """
  @spec call(String.t()) :: LinkPreview.success() | LinkPreview.failure()
  def call(url) do
    case Requests.head(url) do
      {:ok, %Tesla.Env{status: 200, url: final_url} = env} ->
        action_for_content_type(url, final_url, env)

      {:ok, %Tesla.Env{status: 405, url: final_url} = env} ->
        action_for_content_type(url, final_url, env)

      {:ok, _} ->
        %LPError{origin: LinkPreview, message: "Unsupported response"}

      {:error, message} ->
        %LPError{origin: LinkPreview, message: message}
    end
    |> to_tuple()
  catch
    _, %{__struct__: origin, message: message} ->
      {:error, %LPError{origin: origin, message: message}}

    _, _ ->
      {:error, %LPError{origin: :unknown}}
  end

  defp to_tuple(result) do
    case result do
      %Page{} -> {:ok, result}
      %LPError{} -> {:error, result}
    end
  end

  defp action_for_content_type(url, final_url, env) do
    case Tesla.get_header(env, "content-type") do
      "text/html" <> _ ->
        get_html_and_collect_data(url, final_url)

      "image/" <> _ ->
        collect_data(url, final_url, [Image], nil)
    end
  end

  defp get_html_and_collect_data(url, final_url) do
    parsers = Application.get_env(:link_preview, :parsers, [Opengraph, Html])

    case Requests.get(url) do
      {:ok, %Tesla.Env{status: 200, body: body}} ->
        collect_data(url, final_url, parsers, body)

      _ ->
        %LPError{origin: LinkPreview, message: "Unsupported response"}
    end
  end

  defp collect_data(url, final_url, parsers, body) do
    page = Page.new(url, final_url)
    Enum.reduce(parsers, page, &apply_each_function(&1, &2, body))
  end

  defp apply_each_function(parser, page, body) do
    if Code.ensure_loaded?(parser) do
      Enum.reduce_while(Basic.parsable(), page, &apply_or_halt(parser, &1, &2, body))
    else
      page
    end
  end

  defp apply_or_halt(parser, :images, page, body) do
    current_value = Map.get(page, :images)

    if current_value == [] do
      {:cont, apply(parser, :images, [page, body])}
    else
      {:halt, page}
    end
  end

  defp apply_or_halt(parser, function, page, body) do
    current_value = Map.get(page, function)

    if is_nil(current_value) do
      {:cont, apply(parser, function, [page, body])}
    else
      {:halt, page}
    end
  end
end
