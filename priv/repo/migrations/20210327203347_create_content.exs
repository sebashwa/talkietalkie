defmodule Talkietalkie.Repo.Migrations.CreateContents do
  use Ecto.Migration

  def change do
    create table(:contents) do
      add :title, :string
      add :url, :string, null: false
      add :user_id, references(:users, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:contents, [:user_id])
    create unique_index(:contents, [:url])
  end
end
