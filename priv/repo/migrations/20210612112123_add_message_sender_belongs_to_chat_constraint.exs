defmodule Talkietalkie.Repo.Migrations.AddMessageSenderBelongsToChatConstraint do
  use Ecto.Migration

  def change do
    execute(
      ~S"""
      CREATE OR REPLACE FUNCTION check_message_sender_belongs_to_chat()
        RETURNS TRIGGER AS $$
      DECLARE
        requester_id BIGINT;
        responder_id BIGINT;
      BEGIN
        IF NOT EXISTS (
          SELECT 1
          FROM chats
          WHERE chats.id = NEW.chat_id
          AND (chats.requester_id = NEW.sender_id OR chats.responder_id = NEW.sender_id)
        )
        THEN
          RAISE foreign_key_violation
          USING CONSTRAINT = 'message_sender_does_not_belong_to_chat';
        END IF;
      RETURN NEW;
      END;
      $$ language plpgsql;
      """,
      "DROP FUNCTION check_message_sender_belongs_to_chat;"
    )

    execute(
      ~S"""
      CREATE TRIGGER message_sender_belongs_to_chat_check
      BEFORE INSERT ON messages
      FOR EACH ROW
      EXECUTE PROCEDURE check_message_sender_belongs_to_chat();
      """,
      "DROP TRIGGER message_sender_belongs_to_chat_check ON messages;"
    )
  end
end
