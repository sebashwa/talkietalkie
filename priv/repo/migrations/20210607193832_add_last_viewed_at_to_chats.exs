defmodule Talkietalkie.Repo.Migrations.AddLastViewedAtToChats do
  use Ecto.Migration

  def change do
    alter table(:messages) do
      add :read_at, :naive_datetime
    end
  end
end
