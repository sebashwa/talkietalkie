defmodule Talkietalkie.Repo.Migrations.CreateTalkies do
  use Ecto.Migration

  def change do
    create table(:talkies) do
      add :insight_wanted, :boolean, default: false, null: false
      add :knowhow_present, :boolean, default: false, null: false
      add :knowhow_reason, :text
      add :rating, :integer, default: 5, null: false
      add :reason, :text
      add :content_id, references(:contents, on_delete: :delete_all), null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:talkies, [:content_id])
    create index(:talkies, [:user_id])
    create unique_index(:talkies, [:content_id, :user_id], name: :unique_content_for_user)
  end
end
