defmodule Talkietalkie.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :text, :text
      add :chat_id, references(:chats, on_delete: :restrict), null: false
      add :sender_id, references(:users, on_delete: :nilify_all), null: false

      timestamps()
    end

    create(index(:messages, [:chat_id]))
    create(index(:messages, [:sender_id]))
  end
end
