defmodule Talkietalkie.Repo.Migrations.AddRequesterNotResponderConstraintToChats do
  use Ecto.Migration

  def change do
    create constraint(:chats, :requester_not_responder, check: "requester_id != responder_id")
  end
end
