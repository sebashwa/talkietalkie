defmodule Talkietalkie.Repo.Migrations.CreateChats do
  use Ecto.Migration

  def change do
    create table(:chats) do
      add :private, :boolean, default: false, null: false
      add :banned, :boolean, default: false, null: false
      add :requester_id, references(:users, on_delete: :nilify_all), null: false
      add :responder_id, references(:users, on_delete: :nilify_all), null: false
      add :content_id, references(:contents, on_delete: :restrict), null: false

      timestamps()
    end

    create index(:chats, [:requester_id])
    create index(:chats, [:responder_id])
    create index(:chats, [:content_id])

    create unique_index(:chats, [:content_id, :requester_id, :responder_id],
             name: :unique_content_for_users
           )
  end
end
