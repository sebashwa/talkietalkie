# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :talkietalkie,
  ecto_repos: [Talkietalkie.Repo]

# Configures the endpoint
config :talkietalkie, TalkietalkieWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "DBi4MfK6um7gnucKkR4CE04l+9wskXy7XCOm1udP2u9LUEtdFUhFrpTpYA91m0h+",
  render_errors: [view: TalkietalkieWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Talkietalkie.PubSub,
  live_view: [signing_salt: "yYCvPxAQ"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :talkietalkie, TalkietalkieWeb.Gettext, default_locale: "en", locales: ~w(en de)

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
